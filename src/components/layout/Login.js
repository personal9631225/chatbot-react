import logo from "../../assets/img/parsian.png";
import login from "../../assets/img/login.png";

function Login(props) {
    return (
        <div className="h-screen grid grid-cols-1 md:grid-cols-2">
            <div
                className="w-full flex justify-center items-center h-screen bg-[#F6F8FF] overflow-y-hidden md:overflow-y-auto">
                <section className="text-right flex justify-center w-full">
                    <div className="flex flex-col items-center justify-center w-full md:w-auto">
                        <img src={logo}
                             className="h-20"/>
                        <h1 className="mt-5 mb-3 text-2xl text-center">به چت بات خوش آمدید!</h1>
                        {props.children}
                    </div>
                </section>
            </div>
            <div className="hidden md:flex bg-primary-700 flex-col items-center justify-center">
                <img src={login}
                     className="w-[500px]"/>
                <h3 className="text-white-100 mt-10">هرسوالی مربوط به بیمه داری جوابش اینجاست!</h3>
            </div>
        </div>

    )
}

export default Login;